package lang.taxi.hub.rest

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException(message: String) : RuntimeException(message) {
    companion object {

        fun throwIf(condition: Boolean, message: String) {
            if (condition) {
                throw BadRequestException(message)
            }
        }
    }
}
