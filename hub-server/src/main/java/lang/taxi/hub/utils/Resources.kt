package lang.taxi.hub.utils

import org.springframework.core.io.Resource

fun Resource.loadAsString() = org.apache.commons.io.IOUtils.toString(this.inputStream, java.nio.charset.Charset.defaultCharset())
