package lang.taxi.hub.utils

import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

/**
 * Indicates that a field or class should be excluded from
 * equals and hashcode functions provided by Equality
 */
@Target(AnnotationTarget.TYPE, AnnotationTarget.CLASS, AnnotationTarget.FIELD)
annotation class ExcludeFromEquality

/**
 * Utility to provide Lombok-style equals & hashcode functions
 * based off the attributes in a class.
 *
 * examples:
 * // Everything is considered (unless annotated with @ExcludeFromEquality)
 * private val equality = Equality.allProperties(this)
 *
 * // Everything, except the age property is considered (and those annotated with @ExcludeFromEquality)
 * private val equality = Equality.allProperties(this).except(TestDefaultContract::age)
 *
 * // Only the listed properties are considered:
 * private val equality = Equality(this, TestMember::firstName, TestMember::lastName)
 *
 * Once the equality is defined, use it as follows:
 *
 * override fun equals(other: Any?): Boolean = equality.isEqualTo(other)
 * override fun hashCode(): Int = equality.hash()
 */
@ExcludeFromEquality
class Equality<T : Any>(val target: T, vararg val properties: T.() -> Any?) {

   companion object {
      var INITIAL_ODD_NUMBER = 17
      var MULTIPLIER_PRIME = 37
      fun <T : Any> allProperties(target: T): Equality<T> {
         return allProperties(target, { false })
      }

      private fun <T : Any> allProperties(target: T, except: (KProperty1<out T, Any?>) -> Boolean): Equality<T> {
         val properties = target::class.memberProperties
            .filter { it.findAnnotation<ExcludeFromEquality>() == null }
            .filter { !(it.returnType.classifier as KClass<*>).java.isAnnotationPresent(ExcludeFromEquality::class.java) }
            .filter { !except.invoke(it) }
            .map { it.isAccessible = true; it }
            .toTypedArray()
         return Equality(target, *properties as Array<out T.() -> Any?>)
      }
   }

   fun except(vararg properties: T.() -> Any?): Equality<T> {
      return Equality(target, *this.properties.filterNot { properties.contains(it) }.toTypedArray())
   }

   fun isEqualTo(other: Any?): Boolean {
      if (other == null) return false
      if (other === this) return true
      if (other.javaClass != target.javaClass) return false
      return properties.all {
         try {

             @Suppress("UNCHECKED_CAST")
             it.invoke(target) == it.invoke(other as T)
         } catch (e: Exception) {
            log().error("Failed to check equality on field $it")
            throw e
         }
      }
   }

   fun hash(): Int {
      val fields = properties.map { it.invoke(target)?.hashCode() ?: 0 }
      return fields.fold(INITIAL_ODD_NUMBER) { a, b -> MULTIPLIER_PRIME * a + b }
   }
}
