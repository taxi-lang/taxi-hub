package lang.taxi.hub.utils

import java.math.BigDecimal
import java.util.*
import java.util.concurrent.ThreadLocalRandom

fun Iterable<BigDecimal>.sum(): BigDecimal {
   return this.fold(BigDecimal.ZERO, { a, b -> a.add(b) })
}

fun Iterable<Int>.toBigDecimals(): List<BigDecimal> {
   return this.map { it.toBigDecimal() }
}

fun ClosedRange<Int>.random() =
   Random().nextInt(endInclusive - start) + start

fun ClosedRange<Double>.random() =
   ThreadLocalRandom.current().nextDouble(start, endInclusive)

fun <T : Comparable<T>> T.comparesEqualTo(other: T): Boolean {
   return this.compareTo(other) == 0
}

fun BigDecimal.equalsIgnoreScale(other: BigDecimal): Boolean {
   return this.comparesEqualTo(other)
}

inline fun <T> Iterable<T>.sumByBigDecimal(selector: (T) -> BigDecimal): BigDecimal {
   var sum: BigDecimal = BigDecimal.ZERO
   for (element in this) {
      sum += selector(element)
   }
   return sum
}

object Min {
   fun <T : Comparable<T>> of(a: T, b: T): T {
      return if (a <= b) a else b
   }
}

object Max {
   fun <T : Comparable<T>> of(a: T, b: T): T {
      return if (a >= b) a else b
   }
}

object BigDecimals {
   fun min(a: BigDecimal, b: BigDecimal): BigDecimal {
      return if (a <= b) a else b
   }
}
