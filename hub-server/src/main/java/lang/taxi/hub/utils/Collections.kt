package lang.taxi.hub.utils

import org.apache.commons.lang3.RandomUtils.nextInt
import java.util.*

fun <T> sortedSetOf(vararg elements: T): SortedSet<T> {
   return TreeSet(elements.toList())
}

fun <T> Iterable<T>.randomElement(): T {
   val list = this.toList()
   return list.get(nextInt(0, list.size - 1))
}

fun <T> Iterable<T>.randomElements(size: Int): List<T> {
   return (0..size).map { this.randomElement() }
}

fun <A, B> Iterable<Pair<A, B>>.left(): List<A> {
   return this.map { it.first }
}

fun <A, B> Iterable<Pair<A, B>>.right(): List<B> {
   return this.map { it.second }
}


fun <T> List<T>.withAdded(value: T): List<T> {
   return this + listOf(value)
}

fun <T> List<T>.withRemoved(value: T): List<T> {
   return this - listOf(value)
}

fun <T> List<T>.withRemoved(predicate: (T) -> Boolean): List<T> {
   return this - this.filter(predicate)
}
