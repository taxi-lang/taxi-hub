package lang.taxi.hub.utils.time

import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.TemporalAmount
import java.util.*

object Instants {
   /**
    * A persistence-friendly high date for Instants.
    * Instant.MAX cannot be persisted by MySQL.
    * Note, fails with millis.  We should be fine.
    */
   val MAX = Instant.parse("9999-12-31T23:59:59Z")
}

// This class seems like a bad idea - why doesn't it already exists?
// there must be a reason.
data class Timespan(val startInclusive: Instant, val endExclusive: Instant) {
   constructor (startingFrom: Instant, duration: TemporalAmount) : this(startingFrom, startingFrom.plus(duration))

   val minutes = Duration.between(startInclusive, endExclusive).toMinutes()
   val seconds = Duration.between(startInclusive, endExclusive).seconds

   private val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
      .withLocale(Locale.UK)
      .withZone(ZoneId.systemDefault())

   val description = "${formatter.format(startInclusive)} - ${formatter.format(endExclusive)}"
}
