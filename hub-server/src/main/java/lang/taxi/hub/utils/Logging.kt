package lang.taxi.hub.utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun Any.log(): Logger {
   return LoggerFactory.getLogger(this::class.java)
}

inline fun Boolean.logWhenTrue(message:String):Boolean {
   if (this) {
      this@logWhenTrue.log().debug(message)
   }
   return this
}
