package lang.taxi.hub.utils

/**
 * Invokes the callback provided when T is not null.
 * Exactly the same as input?.let { ... }, except a little more readable
 */
inline fun <T:Any, R> whenNotNull(input: T?, callback: (T)->R): R? {
   return input?.let(callback)
}

inline fun <T:Any,R> T?.ifNotNull(callback: (T) -> R):R? {
   return this?.let(callback)
}

fun <T:Any> T?.orElse(other:T):T {
   return this ?: other
}
