package lang.taxi.hub.utils

import org.apache.commons.lang3.RandomStringUtils
import java.time.Clock
import java.util.*
import kotlin.math.absoluteValue

object Ids {
   fun newId() = UUID.randomUUID().toString()
   fun newId(namespace: String) = "${namespace}-${newId()}"
   // Not as unique as newId, but shorter
   fun timestampId(namespace: String = "") = "$namespace-${Clock.systemUTC().millis()}"

   fun timestampHash() = humanReadableHash(Clock.systemUTC().millis().toInt())
   fun shortId(namespace: String = "", length:Int = 8) = "$namespace-${RandomStringUtils.random(length, true, true)}"

   /**
    * Base62 characters table sorted to quickly calculate decimal equivalency by compensating.
    */
   fun base62(source: Int): String {
      val alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray()
      return base(source,alphabet)
   }

   /**
    * Hashes the source int, using an alphabet with
    * values that look similar removed, to reduce issues when
    * reading
    */
   fun humanReadableHash(source:Int):String {

      val alphabet = "2346789ABCDEFGHIJKLMNPQRTUVWXYZ".toCharArray()
      return base(source.absoluteValue,alphabet)
   }

   private fun base(source:Int,alphabet:CharArray):String {
      var value = source
      val sb = StringBuilder(1)
      do {
         sb.insert(0, alphabet[value % alphabet.size])
         value /= alphabet.size
      } while (value > 0)
      return sb.toString()
   }
}
