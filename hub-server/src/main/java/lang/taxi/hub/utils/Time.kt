package lang.taxi.hub.utils

import java.time.Duration
import java.time.Period

fun Int.days(): Period {
   return Period.ofDays(this)
}

fun Int.months(): Period {
   return Period.ofMonths(this)
}

fun Int.weeks(): Period {
   return Period.ofWeeks(this)
}

fun Int.minutes(): Duration {
   return Duration.ofMinutes(this.toLong())
}

fun Int.hours(): Duration {
   return Duration.ofHours(this.toLong())
}
