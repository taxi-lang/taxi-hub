package lang.taxi.hub.utils

import java.util.concurrent.CompletableFuture

/**
 * Collects all the results from a list of completable futures,
 * and returns them in a list
 */
fun <T> List<CompletableFuture<T>>.collate():CompletableFuture<List<T>> {
   return CompletableFuture.allOf(*this.toTypedArray()) // once all the results are completed
      .thenApply { this.map { it.join() } } // collect them all
}
fun <T> List<CompletableFuture<T>>.and() = this.collate()

