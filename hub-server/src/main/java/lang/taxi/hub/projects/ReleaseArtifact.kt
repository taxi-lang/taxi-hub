package lang.taxi.hub.projects

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class ReleaseArtifact(
        @Id val id: String,
        @field:JsonIgnore
        val path: String,
        val name: String,
        @field:JsonIgnore
        @ManyToOne
        val release: Release
)
