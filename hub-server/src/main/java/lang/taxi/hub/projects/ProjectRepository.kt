package lang.taxi.hub.projects

import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface ProjectRepository : JpaRepository<Project, String> {
    fun findByOwnerAndName(owner: String, name: String): Optional<Project>

    fun findByNameIgnoreCaseContaining(searchTerm: String): List<Project>
}
