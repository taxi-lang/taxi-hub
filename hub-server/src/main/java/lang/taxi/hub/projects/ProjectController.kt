package lang.taxi.hub.projects

import lang.taxi.hub.rest.BadRequestException
import lang.taxi.hub.rest.NotFoundException
import lang.taxi.hub.utils.Ids
import lang.taxi.hub.utils.log
import org.springframework.hateoas.Resource
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.web.bind.annotation.*
import java.time.Instant

@RestController
class ProjectController(
   private val repository: ProjectRepository) {

   @RequestMapping(method = [RequestMethod.GET], value = ["/projects"], params = ["q"])
   fun findProjects(@RequestParam("q") searchTerm: String): List<Resource<Project>> {
      return repository.findByNameIgnoreCaseContaining(searchTerm)
         .map { toResource(it) }
   }

   @RequestMapping(method = [RequestMethod.GET], value = ["/projects"])
   fun listProjects(): List<Resource<Project>> {
      // TODO : Filter by what the user is allowed to see
      return repository.findAll()
         .map { toResource(it) }
   }

   private fun toResource(project: Project): Resource<Project> {
      return Resource(project,
         linkTo(methodOn(ProjectController::class.java).get(project.owner, project.name)).withSelfRel()
      )
   }

   fun getOrCreateProject(owner: String, name: String, projectType: ProjectType?): Project {
      val project = repository.findByOwnerAndName(owner, name)
      return project.orElseGet {
         require(projectType != null) { "Project $owner/$name does not exist, but cannot create one as no Project Type was provided"}
         createProject(
            owner,
            name,
            // Default to schema.  This is a change from when this was first written, and we were defaulting
            // to plugins (or not differentiating).  This may cause issues.
            CreateProjectRequest(projectType!!)
         )
      }
   }

   @RequestMapping(method = [RequestMethod.PUT], value = ["/projects/{owner}/{name}"])
   fun createProject(@PathVariable("owner") owner: String, @PathVariable("name") name: String, @RequestBody request: CreateProjectRequest): Project {
      // TODO : Validate that the user is allowed to operate on the owner
      if (repository.findByOwnerAndName(owner, name).isPresent) {
         throw BadRequestException("Project $owner/$name already exists")
      }
      val project = repository.save(Project(
         id = Ids.newId(),
         owner = owner,
         name = name,
         createdAt = Instant.now(),
         projectType = request.projectType
      ))

      log().info("Project {} created", project.identifier)
      return project
   }

   @RequestMapping(method = [RequestMethod.GET], value = ["/projects/{owner}/{name}"])
   fun get(@PathVariable("owner") owner: String, @PathVariable("name") name: String): Project {
      return repository.findByOwnerAndName(owner, name)
         .orElseThrow<RuntimeException> { NotFoundException("No repository $owner/$name was found") }
   }
}

data class CreateProjectRequest(
   val projectType: ProjectType
)
