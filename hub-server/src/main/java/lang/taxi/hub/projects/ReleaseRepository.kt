package lang.taxi.hub.projects

import com.github.zafarkhaja.semver.Version
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ReleaseRepository : JpaRepository<Release, String> {
    fun findByProject(project: Project): List<Release>

    fun findByProjectAndVersion(project: Project, version: String): Optional<Release>
}

fun ReleaseRepository.getLatestRelease(project: Project): Release? {
    return this.findByProject(project).maxBy { Version.valueOf(it.version) }
}
