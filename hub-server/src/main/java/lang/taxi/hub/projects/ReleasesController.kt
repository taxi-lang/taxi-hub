package lang.taxi.hub.projects

import com.github.zafarkhaja.semver.Version
import lang.taxi.hub.api.IRelease
import lang.taxi.hub.rest.BadRequestException
import lang.taxi.hub.rest.NotFoundException
import lang.taxi.hub.storage.ReleaseArtifactStore
import lang.taxi.hub.utils.Ids
import lang.taxi.hub.utils.log
import org.springframework.core.io.FileSystemResource
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.net.URI
import java.time.Instant
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/schemas/{owner}/{name}/releases")
class SchemaReleasesController(projectController: ProjectController,
                               releaseRepository: ReleaseRepository,
                               store: ReleaseArtifactStore) : ReleasesController(projectController, releaseRepository, store) {
   override val projectType: ProjectType = ProjectType.SCHEMA
}


@RestController
@RequestMapping("/plugins/{owner}/{name}/releases")
class PluginReleasesController(projectController: ProjectController,
                               releaseRepository: ReleaseRepository,
                               store: ReleaseArtifactStore) : ReleasesController(projectController, releaseRepository, store) {
   override val projectType: ProjectType = ProjectType.PLUGIN
   override fun getPreferredFileName(file: MultipartFile): String {
      return "plugin.jar"
   }

}

@RestController
abstract class ReleasesController(
   private val projectController: ProjectController,
   private val releaseRepository: ReleaseRepository,
   private val store: ReleaseArtifactStore

) {
   abstract val projectType: ProjectType

   @GetMapping("")
   fun listReleases(@PathVariable("owner") owner: String, @PathVariable("name") name: String): List<IRelease> {
      val project = projectController.get(owner, name)
      return releaseRepository.findByProject(project)
   }

   @RequestMapping(value = "/{version:.+}", method = [RequestMethod.GET])
   fun getRelease(@PathVariable("owner") owner: String, @PathVariable("name") name: String, @PathVariable("version") version: String): ResponseEntity<IRelease> {
      val release = findRelease(owner, name, version)
      return ResponseEntity.ok(release)
   }

   @GetMapping(value = ["/{version:.+}/content"], produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
   fun getReleaseContent(@PathVariable("owner") owner: String, @PathVariable("name") name: String, @PathVariable("version") version: String,
                         response: HttpServletResponse
   ): FileSystemResource {
      val release = findRelease(owner, name, version)
      if (release.artifacts.size != 1) {
         throw BadRequestException("${release.identifier} has multiple files, which must be downloaded separately")
      }
      val file = store.get(URI(release.artifacts.first().path))

      // Set the contentDisposition header donwloads the file with the correct filename
      // Use this apporach to avoid issues with malicious filenames
      val contentDisposition: ContentDisposition = ContentDisposition.builder("inline")
         .filename(file.name)
         .build()

      response.setHeader(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString())
      return FileSystemResource(file)
   }

   private fun findRelease(@PathVariable("owner") owner: String, @PathVariable("name") name: String, @PathVariable("version") version: String): Release {
      val project = projectController.get(owner, name)
      return releaseRepository.findByProjectAndVersion(project, version)
         .orElseThrow<RuntimeException> { NotFoundException("Release " + version + " does not exist for project " + project.identifier) }
   }

   // Note : magic var in mapping is to allow dots in the version.
   fun createRelease(owner: String, name: String, version: String): Release {
      val project = projectController.getOrCreateProject(owner, name, projectType)
      val existing = releaseRepository.findByProjectAndVersion(project, version)
      if (existing.isPresent) {
         throw BadRequestException("$version already exists for $owner/$name")
      }
      val release = Release(
         id = Ids.newId(),
         project = project,
         version = version,
         createdAt = Instant.now(),
         createdBy = "FIXME")


      releaseRepository.save(release)
      log().info("Release {} created", release.identifier)
      return release
   }

   @PostMapping(params = ["releaseType"])
   fun createIncrementalRelease(
      @PathVariable("owner") owner: String,
      @PathVariable("name") name: String,
      @RequestParam("file") file: MultipartFile,
      @RequestParam("releaseType") releaseType: ReleaseType): Release {
      val project = projectController.getOrCreateProject(owner, name, projectType)
      val latestRelease = releaseRepository.getLatestRelease(project)

      val nextVersion = getNextVersionNumber(releaseType, latestRelease)
      val release = createRelease(owner, name, nextVersion)

      return attachFile(file, release)
   }

   private fun getNextVersionNumber(releaseType: ReleaseType, release: Release?): String {
      return if (release != null) {
         val lastVersion = Version.valueOf(release.version)
         val incremented = when (releaseType) {
            ReleaseType.MAJOR -> lastVersion.incrementMajorVersion()
            ReleaseType.MINOR -> lastVersion.incrementMinorVersion()
            ReleaseType.PATCH -> lastVersion.incrementPatchVersion()
         }
         incremented.toString()
      } else {
         "0.1.0"
      }
   }

   @RequestMapping(value = ["/{version:.+}"], method = [RequestMethod.POST])
   fun uploadArtifact(
      @PathVariable("owner") owner: String,
      @PathVariable("name") name: String,
      @PathVariable("version") version: String,
      @RequestParam("file") file: MultipartFile
   ): Release {

      val release = createRelease(owner, name, version)
      return attachFile(file, release)
   }

   private fun attachFile(file: MultipartFile, release: Release): Release {
      val location = store.save(file, release, getPreferredFileName(file))
      val artifact = createReleaseArtifact(location, file, release)
      release.addArtifact(artifact)
      releaseRepository.save(release)

      log().info("Artifact ${artifact.name} uploaded to release ${release.identifier}")
      return release
   }

   fun getPreferredFileName(file: MultipartFile): String {
      return file.originalFilename!!
   }

   fun createReleaseArtifact(location: URI, file: MultipartFile, release: Release): ReleaseArtifact {
      return ReleaseArtifact(
         id = Ids.newId(),
         path = location.toASCIIString(),
         release = release,
         name = File(location).name
      )
   }

}

enum class ReleaseType {
   MAJOR, MINOR, PATCH
}

