package lang.taxi.hub.projects

import com.fasterxml.jackson.annotation.JsonIgnore
import lang.taxi.hub.api.IRelease
import java.time.Instant
import javax.persistence.*

@Entity(name = "project_release")
@Table(indexes = [Index(name = "ix_projectVersionRelease", columnList = "project_id,version")])
class Release(
        @Id
        val id: String,
        @ManyToOne
        val project: Project,
        // Keep this short, as it's part of an index
        @Column(length = 10)
        val version: String,
        val createdAt: Instant,
        val createdBy: String,
        @field:JsonIgnore
        @OneToMany(mappedBy = "release", cascade = [CascadeType.ALL])
        val artifacts: MutableSet<ReleaseArtifact> = mutableSetOf()


) : IRelease {


    val identifier: String = "${project.identifier}/$version"

    fun addArtifact(artifact: ReleaseArtifact) {
        this.artifacts.add(artifact)
    }

    enum class ReleaseStatus {
        /**
         * Artifacts are still being uploaded to this release
         */
        STAGING,

        /**
         * This release has been finalised, no more changes can be made
         */
        FINAL

    }

}
