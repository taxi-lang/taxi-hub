package lang.taxi.hub.projects

import java.time.Instant
import javax.persistence.*

@Entity
@Table(indexes = [Index(name = "ix_projectUniqueness", columnList = "name,owner", unique = true)]
)
data class Project(
   @Id
   @Column(length = 70) // UUID is 60, add a bit
   val id: String,
   val name: String,
   val owner: String,
   val createdAt: Instant,
   @Enumerated(EnumType.STRING)
   val projectType: ProjectType
) {
   val identifier: String = "$owner/$name"
}

enum class ProjectType {
   PLUGIN,
   SCHEMA
}
