package lang.taxi.hub.projects

import lang.taxi.hub.rest.NotFoundException
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import java.io.File
import java.net.URI

@RestController
class ArtifactController(
        val projectRepository: ProjectRepository,
        val releaseRepository: ReleaseRepository
) {

    @GetMapping("/artifacts/{owner}/{name}/{version}")
    @ResponseBody
    fun getArtifact(@PathVariable("owner") owner: String,
                    @PathVariable("name") name: String,
                    @PathVariable("version") version: String): ResponseEntity<FileSystemResource> {
        val project = projectRepository.findByOwnerAndName(owner, name).orElseThrow { NotFoundException("No project found at $owner/$name") }
        val release = if (version.toLowerCase() == "latest") {
            releaseRepository.getLatestRelease(project) ?: throw NotFoundException("No releases yet for $owner/$name")
        } else {
            releaseRepository.findByProjectAndVersion(project, version).orElseThrow { NotFoundException("No releasse $version for $owner/$name") }
        }
        return createResponse(release)
    }

    fun createResponse(release: Release): ResponseEntity<FileSystemResource> {
        require(release.artifacts.size == 1) { "Releases with multiple artifacts not supported" }
        val artifact = release.artifacts.first()

        val file = File(URI.create(artifact.path))
        require(file.exists()) { "Specifed file is no longer present on the filesystem" }
        require(file.length() > 0) { "File appears to be empty"}
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${artifact.name}\"")
                .header("X-Version", release.version)
                .header("X-Artifact-Id", release.identifier)
                .header("X-Filename", artifact.name)
                .body(FileSystemResource(file))
    }
}