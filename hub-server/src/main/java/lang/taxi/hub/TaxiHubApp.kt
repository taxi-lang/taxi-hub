package lang.taxi.hub

import lang.taxi.hub.storage.StoreConfig
import lang.taxi.hub.utils.log
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import java.nio.file.Paths

@SpringBootApplication
class TaxiHubApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(TaxiHubApp::class.java, *args)
        }
    }

    @Value("\${taxihub.store.path}")
    lateinit var fileStorePath: String

    @Bean
    fun localStorePath(): StoreConfig {
        log().info("Using local store at $fileStorePath")
        return StoreConfig(Paths.get(fileStorePath))
    }
}