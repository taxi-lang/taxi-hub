package lang.taxi.hub.storage

import lang.taxi.hub.projects.Release
import org.springframework.web.multipart.MultipartFile

import java.io.File
import java.net.URI

interface ReleaseArtifactStore {
    fun save(file: MultipartFile, release: Release, fileName:String? = null): URI

    operator fun get(uri: URI): File
}
