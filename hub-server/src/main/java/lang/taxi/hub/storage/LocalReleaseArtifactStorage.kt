package lang.taxi.hub.storage

import lang.taxi.hub.projects.Release
import lang.taxi.hub.utils.log
import org.apache.commons.io.FileUtils
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile

import java.io.File
import java.net.URI
import java.nio.file.Path

data class StoreConfig(
   val root: Path
)

@Component
class LocalReleaseArtifactStorage(config: StoreConfig) : ReleaseArtifactStore {
   val root = config.root
   override fun save(file: MultipartFile, release: Release, fileName: String?): URI {
      val path = root.resolve(release.identifier)
      val destFile = if (fileName != null) {
         File(path.toFile(), fileName)
      } else {
         File(path.toFile(), file.originalFilename!!)
      }
      FileUtils.forceMkdir(path.toFile())
      FileUtils.writeByteArrayToFile(destFile, file.bytes)
      log().info("Saved new file to {}", destFile.canonicalPath)
      return destFile.toURI()
   }

   override fun get(uri: URI): File {
      return File(uri)
   }
}
